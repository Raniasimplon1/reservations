<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Personne")
 */
class Personne {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    public $Nom;
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    public $Prenom;
    
    /**
     * @ORM\Column(type="date")
     */
    public $depart;
    
    /**
     * @ORM\Column(type="date")
     */
    public $retour;
    
    
    
    
    
    /**
     * @ORM\ManyToOne(targetEntity="moyen", inversedBy="moyen")
      
    * @ORM\JoinColumn(name="moyen_id", referencedColumnName="id", nullable=false)
*/ 
    protected $moyen;
    /**
     * @ORM\ManyToOne(targetEntity="ville", inversedBy="ville")
     
     * @ORM\JoinColumn(name="ville_id", referencedColumnName="id", nullable=false)
     */
    protected $ville;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Personne
     */
    public function setNom($nom)
    {
        $this->Nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->Nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Personne
     */
    public function setPrenom($prenom)
    {
        $this->Prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->Prenom;
    }

    /**
     * Set depart
     *
     * @param \DateTime $depart
     *
     * @return Personne
     */
    public function setDepart($depart)
    {
        $this->depart = $depart;

        return $this;
    }

    /**
     * Get depart
     *
     * @return \DateTime
     */
    public function getDepart()
    {
        return $this->depart;
    }

    /**
     * Set retour
     *
     * @param \DateTime $retour
     *
     * @return Personne
     */
    public function setRetour($retour)
    {
        $this->retour = $retour;

        return $this;
    }

    /**
     * Get retour
     *
     * @return \DateTime
     */
    public function getRetour()
    {
        return $this->retour;
    }

    /**
     * Set moyen
     *
     * @param \AppBundle\Entity\moyen $moyen
     *
     * @return Personne
     */
    public function setMoyen(\AppBundle\Entity\moyen $moyen)
    {
        $this->moyen = $moyen;

        return $this;
    }

    /**
     * Get moyen
     *
     * @return \AppBundle\Entity\moyen
     */
    public function getMoyen()
    {
        return $this->moyen;
    }

    /**
     * Set ville
     *
     * @param \AppBundle\Entity\ville $ville
     *
     * @return Personne
     */
    public function setVille(\AppBundle\Entity\ville $ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return \AppBundle\Entity\ville
     */
    public function getVille()
    {
        return $this->ville;
    }
}
