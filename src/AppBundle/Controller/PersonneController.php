<?php
namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AppBundle\Entity\Personne;
use AppBundle\Entity\moyen;
use AppBundle\Entity\ville;

class PersonneController extends Controller {
/**
* @Route("/create-personne")
*/
public function createAction(Request $request) {

  $personne = new Personne();
  $form = $this->createFormBuilder($personne)
    ->add('nom', TextType::class)
    ->add('prenom', TextType::class)
    ->add('depart', DateType::class)
    ->add('retour', DateType::class)
    ->add('save', SubmitType::class, array('label' => 'Save'))
    ->add('ville', EntityType::class,array( 'class' => 'AppBundle:ville', 'choice_label' => 'ville', ))
    ->add('moyen', EntityType::class,array( 'class' => 'AppBundle:moyen', 'choice_label' => 'moyen', ))
   
    ->getForm();

  $form->handleRequest($request);

  if ($form->isSubmitted()) {

    $personne = $form->getData();

    $em = $this->getDoctrine()->getManager();
    $em->persist($personne);
    $em->flush();

    return $this->redirect('/show-personnes');

  }

  return $this->render(
    'Personne/edit.html.twig',
    array('form' => $form->createView())
    );

}
/**
* @Route("/view-personne/{id}")
*/
public function viewAction($id) {
    
    $personne = $this->getDoctrine()
    ->getRepository('AppBundle:Personne')
    ->find($id);
    
    if (!$personne) {
        throw $this->createNotFoundException(
            'There are no peoples with the following id: ' . $id
            );
    }
    
    return $this->render(
        'Personne/view.html.twig',
        array('personne' => $personne)
        );
    
}

/**
 * @Route("/show-personnes")
 */
public function showAction() {
    
    $personnes = $this->getDoctrine()
    ->getRepository('AppBundle:Personne')
    ->findAll();
    
    return $this->render(
        'Personne/show.html.twig',
        array('personnes' => $personnes)
        );
    
}
    
    
}
